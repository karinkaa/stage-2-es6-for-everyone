import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    const playerOne = {...firstFighter};
    const playerTwo = {...secondFighter};

    playerOne.isBlocked = false;
    playerTwo.isBlocked = false;
    playerOne.timeOfCriticalHit = 0;
    playerTwo.timeOfCriticalHit = 0;

    const playerOneHealth = playerOne.health;
    const playerTwoHealth = playerTwo.health;
    const playerOneHealthIndicator = document.getElementById("left-fighter-indicator");
    const playerTwoHealthIndicator = document.getElementById("right-fighter-indicator");

    let pressed = new Set();

    const keydown = function(event) {
      if (event.repeat) return;

      const code = event.code;
      pressed.add(code);

      const isTimeForPlayerOne = ((new Date().getTime() - playerOne.timeOfCriticalHit) / 1000) > 10;
      const isTimeForPlayerTwo = ((new Date().getTime() - playerTwo.timeOfCriticalHit) / 1000) > 10;

      if (isTimeForPlayerOne && isAllCodesPressed(pressed, controls.PlayerOneCriticalHitCombination)) {
        playerTwo.health -= getCriticalHitPower(playerOne);
        playerTwoHealthIndicator.style.width = getPercentageOfNumber(playerTwo.health, playerTwoHealth) + "%";
        playerOne.timeOfCriticalHit = new Date().getTime();
      } else if (isTimeForPlayerTwo && isAllCodesPressed(pressed, controls.PlayerTwoCriticalHitCombination)) {
        playerOne.health -= getCriticalHitPower(playerTwo);
        playerOneHealthIndicator.style.width = getPercentageOfNumber(playerOne.health, playerOneHealth) + "%";
        playerTwo.timeOfCriticalHit = new Date().getTime();
      } else if (code === controls.PlayerOneAttack && !playerOne.isBlocked && !playerTwo.isBlocked) {
        playerTwo.health -= getDamage(playerOne, playerTwo);
        playerTwoHealthIndicator.style.width = getPercentageOfNumber(playerTwo.health, playerTwoHealth) + "%";
      } else if (code === controls.PlayerTwoAttack && !playerOne.isBlocked && !playerTwo.isBlocked) {
        playerOne.health -= getDamage(playerTwo, playerOne);
        playerOneHealthIndicator.style.width = getPercentageOfNumber(playerOne.health, playerOneHealth) + "%";
      } else if (code === controls.PlayerOneBlock) {
        playerOne.isBlocked = true;
      } else if (code === controls.PlayerTwoBlock) {
        playerTwo.isBlocked = true;
      }

      if (playerOne.health <= 0) {
        document.removeEventListener("keydown", keydown);
        document.removeEventListener("keyup", keyup);
        resolve(secondFighter);
      }
      if (playerTwo.health <= 0) {
        document.removeEventListener("keydown", keydown);
        document.removeEventListener("keyup", keyup);
        resolve(firstFighter);
      }
    }

    const keyup = function(event) {
      pressed.delete(event.code);

      if (event.code === controls.PlayerOneBlock) {
        playerOne.isBlocked = false;
      }
      if (event.code === controls.PlayerTwoBlock) {
        playerTwo.isBlocked = false;
      }
    }

    document.addEventListener('keydown', keydown);
    document.addEventListener('keyup', keyup);
  });
}

export function getDamage(attacker, defender) {
  const hitPower = getHitPower(attacker);
  const blockPower = getBlockPower(defender);

  return blockPower > hitPower ? 0 : hitPower - blockPower;
}

export function getHitPower(fighter) {
  const criticalHitChance = Math.random() + 1;
  return fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter) {
  const dodgeChance = Math.random() + 1;
  return fighter.defense * dodgeChance;
}

export function getCriticalHitPower(fighter) {
  return 2 * fighter.attack;
}

function getPercentageOfNumber(firstNumber, secondNumber) {
  if (firstNumber <= 0) return 0;
  return firstNumber * 100 / secondNumber;
}

function isAllCodesPressed(pressed, codes) {
  for (let code of codes) {
    if (!pressed.has(code)) {
      return false;
    }
  }
  return true;
}