import { showModal } from './modal';
import { createFighterPreview } from '../fighterPreview';

export function showWinnerModal(fighter) {
    showModal({ title: fighter.name, bodyElement: createFighterPreview(fighter) });
}
