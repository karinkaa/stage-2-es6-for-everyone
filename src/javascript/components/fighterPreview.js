import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  if (fighter) {
    const { name, health, attack, defense, source} = fighter;

    const image = createElement({
      tagName: 'img',
      className: 'preview-container___versus-img',
      attributes: {
        src: source,
      }
    });

    fighterElement.append(image);
    fighterElement.append(JSON.stringify({ name, health, attack, defense }, null, 5));
  }

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
